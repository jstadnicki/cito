﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cito.Startup))]
namespace Cito
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
