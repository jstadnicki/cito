﻿namespace Cito
{
    using System.Web;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Models;
    using Services;

    public class AccountAdapter : IAccount
    {
        public string GetCurrentUserId()
        {
            return AuthenticationManager.User.Identity.GetUserId();
        }

        public ApplicationUser GetUserModel(string userId)
        {
            var model = UserManager.FindById(userId);
            return model;
        }

        public string GetCurrentUserName()
        {
            return AuthenticationManager.User.Identity.Name;
        }

        public string GetCurrentUserEmail()
        {
            var userid = AuthenticationManager.User.Identity.GetUserId();
            var applicationUser = this.UserManager.FindById(userid);
            return applicationUser.Email;
        }

        public string GetCurrentUserPhone()
        {
            var userid = AuthenticationManager.User.Identity.GetUserId();
            var applicationUser = this.UserManager.FindById(userid);
            return applicationUser.PhoneNumber;
        }


        private ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
    }
}