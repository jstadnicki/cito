namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "ViewModel_LoginViewModel_Email", ResourceType = typeof(Translations))]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "ViewModel_LoginViewModel_Password", ResourceType = typeof(Translations))]
        public string Password { get; set; }

        [Display(Name = "ViewModel_LoginViewModel_RememberMe", ResourceType = typeof(Translations))]
        public bool RememberMe { get; set; }
    }
}