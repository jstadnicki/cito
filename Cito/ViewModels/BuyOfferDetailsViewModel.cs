﻿namespace Cito.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web;
    using Microsoft.AspNet.Identity;
    using Resources;

    public class BuyOfferDetailsViewModel
    {
        public long Id { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_Title", ResourceType = typeof(Translations))]
        public string Title { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_Description", ResourceType = typeof(Translations))]
        public string Description { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_Created", ResourceType = typeof(Translations))]
        public DateTime Created { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_DeliverToCity", ResourceType = typeof(Translations))]
        public string DeliverToCity { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_BuyerName", ResourceType = typeof(Translations))]
        public string BuyerName { get; set; }

        public string BuyerId { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_SellOffers", ResourceType = typeof(Translations))]
        public List<SellOfferDetailsViewModel> SellOffers { get; set; }

        [Display(Name = "ViewModel_BuyOfferDetailsViewModel_ValidUntil", ResourceType = typeof(Translations))]
        public DateTime ValidUntil { get; set; }

        public bool IsCurrentUserIsBuyer()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated &&
                   (HttpContext.Current.User.Identity.GetUserId() == this.BuyerId);
        }

        public bool CanPostSellOffers()
        {
            return HttpContext.Current.User.Identity.IsAuthenticated &&
                   this.BuyerId != HttpContext.Current.User.Identity.GetUserId();
        }
    }
}