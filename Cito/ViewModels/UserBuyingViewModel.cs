﻿namespace Cito.ViewModels
{
    using System.Collections.Generic;

    public class UserBuyingViewModel
    {
        public List<BuyOfferDetailsViewModel> BuyOfferList { get; set; }
    }
}