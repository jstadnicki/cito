﻿namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class ManageEmailChangeViewModel
    {
        public ManageEmailChangeViewModel(string email)
        {
            this.CurrentUserEmail = email;
            this.NewEmailAddress = string.Empty;
        }

        [Display(Name = "ViewModel_ManageEmailChangeViewModel_NewEmailAddress", ResourceType = typeof(Translations))]
        public string NewEmailAddress { get; set; }

        [Display(Name = "ViewModel_ManageEmailChangeViewModel_CurrentUserEmail", ResourceType = typeof(Translations))]
        public string CurrentUserEmail { get; set; }
    }
}