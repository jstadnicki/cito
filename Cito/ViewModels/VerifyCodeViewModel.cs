namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "ViewModel_VerifyCodeViewModel_Code", ResourceType = typeof(Translations))]
        public string Code { get; set; }

        public string ReturnUrl { get; set; }

        [Display(Name = "ViewModel_VerifyCodeViewModel_RememberBrowser", ResourceType = typeof(Translations))]
        public bool RememberBrowser { get; set; }

        [Display(Name = "ViewModel_VerifyCodeViewModel_RememberMe", ResourceType = typeof(Translations))]
        public bool RememberMe { get; set; }
    }
}