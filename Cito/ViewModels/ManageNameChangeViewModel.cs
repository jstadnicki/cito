﻿namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class ManageNameChangeViewModel
    {
        public ManageNameChangeViewModel(string currentUserName)
        {
            this.CurrentUserName = currentUserName;
            this.NewUserName = string.Empty;
        }


        [Display(Name = "Aktualna nazwa użytkownika")]
        public string CurrentUserName { get; set; }

        [Required]
        [MinLength(3, ErrorMessageResourceName = "ViewModel_ManageNameChangeViewModel_NewUserName", ErrorMessageResourceType = typeof(Translations))]
        [Display(Name = "Nowa nazwa użytkownika")]
        public string NewUserName { get; set; }
    }
}