using System;

namespace Cito.ViewModels
{
    using System.Collections.Generic;
    using Models;

    public class OfferListViewModel
    {
        public List<BuyOfferDetailsViewModel> BuyOfferList { get; set; }
        public long TotalActiveOffers { get; set; }
        public DisplayListQueryParameters ActiveQuery { get; set; }

        public int CurrentPage => this.ActiveQuery.Page;
        public long PagesCount => (this.TotalActiveOffers / this.ActiveQuery.Show) + 1;

        public List<NavigationPage> NavigationPages()
        {
            var min = Math.Max(1, this.ActiveQuery.Page - 3);
            var max = Math.Min(PagesCount, this.ActiveQuery.Page + 3);

            var list = new List<NavigationPage>();

            for (int i = min; i <= max; i++)
            {
                list.Add(new NavigationPage(i, CreateNavigationPageCssClass(i,this.ActiveQuery.Page)));
            }
            return list;
        }

        private string CreateNavigationPageCssClass(int index, int currentPage)
        {
            return index == currentPage ? "active" : string.Empty;
        }

        public class NavigationPage
        {
            public NavigationPage(int page, string cssClass)
            {
                this.Page = page;
                this.CssClass = cssClass;
            }

            public int Page { get; set; }
            public string CssClass { get; set; }
        }

        public string PrevPageCssClass()
        {
            return this.CurrentPage <= 1 ? "disabled" : string.Empty;
        }

        public string NextPageCssClass()
        {
            return this.CurrentPage >= PagesCount ? "disabled" : string.Empty;
        }
    }
}