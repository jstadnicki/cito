﻿namespace Cito.ViewModels
{
    public class ManagePhoneChangeViewModel
    {
        public ManagePhoneChangeViewModel(string currentUserPhone)
        {
            CurrentUserPhone = currentUserPhone;
        }

        public string CurrentUserPhone { get; set; }
        public string NewPhoneNumber { get; set; }
    }
}