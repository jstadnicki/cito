namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessageResourceName = "ViewModel_SetPasswordViewModel_NewPassword_ErrorMessage", MinimumLength = 6, ErrorMessageResourceType = typeof(Translations))]
        [DataType(DataType.Password)]
        [Display(Name = "ViewModel_SetPasswordViewModel_NewPassword", ResourceType = typeof(Translations))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ViewModel_SetPasswordViewModel_ConfirmPassword", ResourceType = typeof(Translations))]
        [Compare("NewPassword", ErrorMessageResourceName = "ViewModel_SetPasswordViewModel_ConfirmPassword_ErrorMessage", ErrorMessageResourceType = typeof(Translations))]
        public string ConfirmPassword { get; set; }
    }
}