﻿namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class UserContactDetailsViewModel
    {
        public string Id { get; set; }

        [Display(Name = "ViewModel_UserContactDetailsViewModel_Name", ResourceType = typeof(Translations))]
        public string Name { get; set; }

        [Display(Name = "ViewModel_UserContactDetailsViewModel_Email", ResourceType = typeof(Translations))]
        public string Email { get; set; }

        [Display(Name = "ViewModel_UserContactDetailsViewModel_PhoneNumber", ResourceType = typeof(Translations))]
        public string PhoneNumber { get; set; }
    }
}