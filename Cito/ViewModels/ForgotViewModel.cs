namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "ViewModel_ForgotViewModel_Email", ResourceType = typeof(Translations))]
        public string Email { get; set; }
    }
}