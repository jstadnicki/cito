namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class ChangePasswordViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ViewModel_ChangePasswordViewModel_OldPassword_Required")]
        [DataType(DataType.Password)]
        [Display(Name = "ViewModel_ChangePasswordViewModel_OldPassword", ResourceType = typeof(Translations))]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ViewModel_ChangePasswordViewModel_NewPassword_Required")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "ViewModel_ChangePasswordViewModel_NewPassword", ResourceType = typeof(Translations))]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        [Display(Name = "ViewModel_ChangePasswordViewModel_ConfirmPassword", ResourceType = typeof(Translations))]
        public string ConfirmPassword { get; set; }
    }
}