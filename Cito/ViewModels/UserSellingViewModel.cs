﻿namespace Cito.ViewModels
{
    using System.Collections.Generic;

    public class UserSellingViewModel
    {
        public List<SellOfferDetailsViewModel> SellOfferList { get; set; }
    }
}