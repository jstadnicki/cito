﻿namespace Cito.ViewModels
{
    using System.Collections.Generic;

    public class HomeIndexViewModel
    {
        public List<BuyOfferDetailsViewModel> BuyOfferList { get; set; }
    }
}