namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "ViewModel_RegisterViewModel_Email", ResourceType = typeof(Translations))]
        public string Email { get; set; }

        [Required]
        [MinLength(3, ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ViewModel_RegisterViewModel_Name_MinLength")]
        [Display(Name = "ViewModel_RegisterViewModel_Name", ResourceType = typeof(Translations))]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessageResourceName = "ViewModel_RegisterViewModel_Password_StringLength", MinimumLength = 6, ErrorMessageResourceType = typeof(Translations))]
        [Display(Name = "ViewModel_RegisterViewModel_Password", ResourceType = typeof(Translations))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ViewModel_RegisterViewModel_ConfirmPassword", ResourceType = typeof(Translations))]
        [Compare("Password", 
            ErrorMessageResourceType = typeof(Translations), 
            ErrorMessageResourceName = "ViewModel_RegisterViewModel_ConfirmPassword_Compare")]
        public string ConfirmPassword { get; set; }
    }
}