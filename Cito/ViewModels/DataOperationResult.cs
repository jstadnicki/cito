﻿namespace Cito.ViewModels
{
    public class DataOperationResult<T>
    {
        public T Data { get; set; }
        public bool IsSuccess { get; set; }

        public static DataOperationResult<T> Success(T data)
        {
            return new DataOperationResult<T> { Data = data, IsSuccess = true };
        }
    }
}