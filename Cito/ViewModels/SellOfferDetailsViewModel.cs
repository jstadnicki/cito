﻿namespace Cito.ViewModels
{
    using System;
    using Models;

    public class SellOfferDetailsViewModel
    {
        public long Id { get; set; }
        public DateTime Created { get; set; }
        public string OwnerId { get; set; }
        public long BuyOfferId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string DeliveryMethod { get; set; }

        public BuyOffer BuyOffer { get; set; }
        public ApplicationUser Owner { get; set; }
    }
}
