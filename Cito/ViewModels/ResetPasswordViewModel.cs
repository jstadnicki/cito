namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Adres email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} musi mie� conajmniej {1} i maksymalnie {2} znak�w.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Has�o")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Potwierd� has�o")]
        [Compare("Has�o", ErrorMessage = "Podane has�a nie zgadzaj� si�, wprowad� je ponownie.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}