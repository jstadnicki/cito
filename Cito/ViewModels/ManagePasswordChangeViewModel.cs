﻿namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class ManagePasswordChangeViewModel
    {
        [Display(Name = "ViewModel_ManagePasswordChangeViewModel_NewPassword", ResourceType = typeof(Translations))]
        public string NewPassword { get; set; }

        [Display(Name = "ViewModel_ManagePasswordChangeViewModel_NewPasswordConfirm", ResourceType = typeof(Translations))]
        public string NewPasswordConfirm { get; set; }

        [Display(Name = "ViewModel_ManagePasswordChangeViewModel_CurrentPassword", ResourceType = typeof(Translations))]
        public string CurrentPassword { get; set; }
    }
}