namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "ViewModel_ForgotPasswordViewModel_Email", ResourceType = typeof(Translations))]
        public string Email { get; set; }
    }
}