﻿namespace Cito.ViewModels
{
    public class OperationResult
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }

        private OperationResult()
        {
            this.Error = string.Empty;
        }

        public static OperationResult Success()
        {
            return new OperationResult { IsSuccess = true };
        }

        public static OperationResult Fail(string errorMessage)
        {
            return new OperationResult {IsSuccess = false, Error = errorMessage};
        }
    }
}