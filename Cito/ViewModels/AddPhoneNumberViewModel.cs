namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class AddPhoneNumberViewModel
    {
        [Phone]
        [Required(ErrorMessageResourceType = typeof(Translations), ErrorMessageResourceName = "ViewModel_AddPhoneNumberViewModel_Number_Required")]
        [Display(Name = "ViewModel_AddPhoneNumberViewModel_Number", ResourceType = typeof(Translations))]
        public string Number { get; set; }
    }
}