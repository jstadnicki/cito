﻿using Cito.Resources;

namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;

    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "ViewModel_ExternalLoginConfirmationViewModel_Email", ResourceType = typeof(Translations))]
        public string Email { get; set; }

        [Required]
        [Display(Name = "ViewModel_ExternalLoginConfirmationViewModel_Name", ResourceType = typeof(Translations))]
        public string Name { get; set; }
    }
}
