namespace Cito.ViewModels
{
    using System.ComponentModel.DataAnnotations;
    using Resources;

    public class VerifyPhoneNumberViewModel
    {
        [Required(ErrorMessageResourceName = "ViewModel_VerifyPhoneNumberViewModel_Code_Required", ErrorMessageResourceType = typeof(Translations))]
        [Display(Name = "ViewModel_VerifyPhoneNumberViewModel_Code", ResourceType = typeof(Translations))]
        public string Code { get; set; }

        [Phone]
        [Required(ErrorMessageResourceName = "ViewModel_VerifyPhoneNumberViewModel_PhoneNumber_Required", ErrorMessageResourceType = typeof(Translations))]
        [Display(Name = "ViewModel_VerifyPhoneNumberViewModel_PhoneNumber", ResourceType = typeof(Translations))]
        public string PhoneNumber { get; set; }
    }
}