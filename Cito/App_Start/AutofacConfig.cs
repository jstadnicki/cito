using Autofac.Builder;
using Cito.Repositories;
using Cito.Services;

namespace Cito
{
    using System.Web.Mvc;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Controllers;
    using Models;

    public class AutofacConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly).InstancePerHttpRequest();

            builder.RegisterType<OfferService>().As<IOfferService>();

            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<AccountService>().As<IAccountService>();

            builder.RegisterType<ApplicationDbContext>().As<IOfferRepository>();
            builder.RegisterType<ApplicationDbContext>().As<IUserRepository>();

            builder.RegisterType<AccountAdapter>().As<IAccount>();
            builder.RegisterType<ConfigurationReader>().As<IConfigurationReader>();


            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}