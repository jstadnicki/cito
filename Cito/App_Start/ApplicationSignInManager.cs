using System.Security.Claims;
using System.Threading.Tasks;
using Cito.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;

namespace Cito
{
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(
            ApplicationUserManager userManager,
            IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }

        public async Task<SignInStatus> LoginByEmailAsync(string email, string password, bool rememberMe, bool shouldLockout)
        {
            var applicationUser = await UserManager.FindByEmailAsync(email);
            if (applicationUser != null)
            {
                var passwordSignInAsync =
                    await this.PasswordSignInAsync(applicationUser.UserName, password, rememberMe, shouldLockout);
                return passwordSignInAsync;
            }

            return await Task.Run(() => SignInStatus.Failure);

        }
    }
}