﻿namespace Cito
{
    using System.Collections.Generic;
    using AutoMapper;
    using Controllers;
    using Dtos;
    using Models;
    using ViewModels;

    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<NewBuyOfferDto, BuyOffer>();
            Mapper.CreateMap<NewSellOfferDto, SellOffer>();
            Mapper.CreateMap<BuyOffer, BuyOfferDetailsViewModel>()
                .ForMember(d => d.BuyerName, o => o.MapFrom(s => s.Owner.UserName))
                .ForMember(d => d.BuyerId, o => o.MapFrom(s => s.Owner.Id));


            Mapper.CreateMap<List<BuyOffer>, UserBuyingViewModel>()
                .ForMember(d => d.BuyOfferList, o => o.MapFrom(s => s));

            Mapper.CreateMap<List<SellOffer>, UserSellingViewModel>()
                .ForMember(d => d.SellOfferList, o => o.MapFrom(s => s));
            Mapper.CreateMap<SellOffer, SellOfferDetailsViewModel>();

            Mapper.CreateMap<List<BuyOffer>, OfferListViewModel>()
                .ForMember(d => d.BuyOfferList, o => o.MapFrom(s => s));

            Mapper.CreateMap<List<BuyOffer>, HomeIndexViewModel>()
                .ForMember(d => d.BuyOfferList, o => o.MapFrom(s => s));

            Mapper.CreateMap<ApplicationUser, UserContactDetailsViewModel>()
                .ForMember(d => d.Name, o => o.MapFrom(s => s.UserName));

            Mapper.CreateMap<ApplicationUser, ManageIndexViewModel>();
        }
    }
}