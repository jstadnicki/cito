﻿using Cito.Dtos;
using Cito.Services;
using Cito.ViewModels;

namespace Cito.Controllers
{
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;

    [Authorize]
    public class ManageController : Controller
    {
        private readonly IAccountService accountService;

        public ManageController(IAccountService accountService)
        {
            this.accountService = accountService;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public async Task<ActionResult> Index()
        {
            var viewmodel = this.accountService.GetCurrentUserDetails();
            return this.View("Index", viewmodel);
        }

        [HttpGet]
        public ActionResult NameChange()
        {
            var viewmodel = this.accountService.GetChangeNameViewModel();
            return this.View("NameChange", viewmodel);
        }

        [HttpPost]
        public ActionResult NameChange(ManageNameChangeDto model)
        {
            if (ModelState.IsValid)
            {
                var operationResult = this.accountService.ChangeCurrentUsername(model);
                if (operationResult.IsSuccess)
                {
                    var user = this.UserManager.FindById(User.Identity.GetUserId());
                    if (user != null)
                    {
                        this.SignInManager.SignInAsync(user, false, false);
                    }
                    return this.RedirectToAction("Index");
                }

                this.ModelState.AddModelError(string.Empty, operationResult.Error);
            }

            var viewmodel = this.accountService.GetChangeNameViewModel();
            viewmodel.NewUserName = model.NewUserName;
            return this.View("NameChange", viewmodel);
        }

        [HttpGet]
        public ActionResult EmailChange()
        {
            ManageEmailChangeViewModel viewmodel = this.accountService.GetChangeEmailViewModel();
            return this.View("EmailChange", viewmodel);
        }

        [HttpPost]
        public ActionResult EmailChange(ManageEmailChangeDto model)
        {
            if (ModelState.IsValid)
            {
                var operationResult = this.accountService.ChangeCurrentUserEmail(model);
                if (operationResult.IsSuccess)
                {
                    var user = this.UserManager.FindById(User.Identity.GetUserId());
                    if (user != null)
                    {
                        this.SignInManager.SignInAsync(user, false, false);
                    }
                    return this.RedirectToAction("Index");
                }

                this.ModelState.AddModelError(string.Empty, operationResult.Error);
            }

            var viewmodel = this.accountService.GetChangeEmailViewModel();
            viewmodel.NewEmailAddress = model.NewEmailAddress;
            return this.View("EmailChange", viewmodel);

        }

        [HttpGet]
        public ActionResult PasswordChange()
        {
            var viewmodel = this.accountService.GetChangePasswordViewModel();
            return this.View("PasswordChange", viewmodel);
        }

        [HttpPost]
        public ActionResult PasswordChange(ManagePasswordChangeDto model)
        {
            if (ModelState.IsValid)
            {
                var operationResult = this.accountService.ChangeCurrentUserpassword(model);
                if (operationResult.IsSuccess)
                {
                    return this.RedirectToAction("Index");
                }

                this.ModelState.AddModelError(string.Empty, operationResult.Error);
            }

            var viewmodel = this.accountService.GetChangePasswordViewModel();
            return this.View("PasswordChange", viewmodel);

        }

        [HttpGet]
        public ActionResult PhoneChange()
        {
            var viewmodel = this.accountService.GetPhoneChangeViewModel();
            return this.View("PhoneChange", viewmodel);
        }

        [HttpPost]
        public ActionResult PhoneChange(ManagePhoneChangeDto model)
        {
            if (ModelState.IsValid)
            {
                var operationResult = this.accountService.ChangeCurrentUserPhone(model);
                if (operationResult.IsSuccess)
                {
                    var user = this.UserManager.FindById(User.Identity.GetUserId());
                    if (user != null)
                    {
                        this.SignInManager.SignInAsync(user, false, false);
                    }
                    return this.RedirectToAction("Index");
                }

                this.ModelState.AddModelError(string.Empty, operationResult.Error);
            }

            var viewmodel = this.accountService.GetPhoneChangeViewModel();
            return this.View("PhoneChange", viewmodel);

        }
    }
}