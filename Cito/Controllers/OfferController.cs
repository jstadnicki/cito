﻿using System;
using System.ComponentModel;
using System.Linq;
using WebGrease.Css.Extensions;

namespace Cito.Controllers
{
    using System.Web.Mvc;
    using Dtos;
    using Models;
    using Services;

    public class OfferController : Controller
    {
        private readonly IOfferService offerService;

        public OfferController(IOfferService offerService)
        {
            this.offerService = offerService;
        }

        [HttpGet]
        public ActionResult List(DisplayListQueryParameters query)
        {
            var viewmodel = this.offerService.GetActiveBuyOffers(query);
            return this.View("List", viewmodel);
        }

        [HttpGet]
        [Authorize]
        public ActionResult NewBuy()
        {
            var model = new NewBuyOfferDto();
            return View("NewBuy", model);
        }


        [HttpPost]
        [Authorize]
        public ActionResult NewBuy(NewBuyOfferDto model)
        {
            if (ModelState.IsValid)
            {
                var result = this.offerService.AddNewBuy(model);
                return this.RedirectToAction("Details", new { id = result.Data });
            }

            return View("NewBuy", model);
        }

        public ActionResult Details(long id)
        {
            var viewmodel = this.offerService.GetBuyOfferDetailsViewModel(id);
            return this.View("Details", viewmodel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult NewSell(long id)
        {
            var model = new NewSellOfferDto(id);
            return this.View("NewSell", model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult NewSell(NewSellOfferDto model)
        {
            if (ModelState.IsValid)
            {
                var result = this.offerService.AddNewSell(model);
                return this.RedirectToAction("Details", new { id = model.BuyOfferId });
            }

            return View("NewSell", model);
        }
    }
}