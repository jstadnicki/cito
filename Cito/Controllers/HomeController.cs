﻿namespace Cito.Controllers
{
    using System.Web.Mvc;
    using Dtos;
    using Services;

    public class HomeController : Controller
    {
        private readonly IOfferService offerService;

        public HomeController(IOfferService offerService)
        {
            this.offerService = offerService;
        }

        public ActionResult Index()
        {
            var viewmodel = this.offerService.GetLatestOffers(9);
            return this.View("Index", viewmodel);
        }

        public ActionResult GenerateBuyOffers(int count)
        {
            for (int i = 0; i < count; i++)
            {
                var dto = new NewBuyOfferDto
                {
                    DeliverToCity = Faker.LocationFaker.City(),
                    Description = Faker.TextFaker.Sentences(5),
                    Title = Faker.TextFaker.Sentence()
                };

                this.offerService.AddNewBuy(dto);
            }
            return this.RedirectToAction("Index");

        }

        public ActionResult GenerateSellOffers()
        {
            return this.RedirectToAction("Index");

        }
    }
}