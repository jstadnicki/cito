﻿namespace Cito.Controllers
{
    using System.ComponentModel;
    using System.Web;
    using System.Web.Mvc;

    public class CustomDefaultModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext,
                                             ModelBindingContext bindingContext,
                                             PropertyDescriptor propertyDescriptor)
        {
            try
            {
                base.BindProperty(controllerContext, bindingContext, propertyDescriptor);
            }
            catch (HttpRequestValidationException e)
            {   
                bindingContext.ModelState.AddModelError(propertyDescriptor.Name, "Wykryto znaki które nie są dopuszczalne.");
            }
        }
    }
}