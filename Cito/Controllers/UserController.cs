﻿using System;

namespace Cito.Controllers
{
    using System.Web.Mvc;
    using Services;

    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        public ActionResult Index()
        {
            return this.View("Index");
        }

        public ActionResult Buying()
        {
            var viewmodel = this.userService.GetBuyOffers();
            return this.View("Buying", viewmodel);
        }

        public ActionResult Selling()
        {
            var viewmodel = this.userService.GetSellOffers();
            return this.View("Selling", viewmodel);
        }

        public ActionResult Contact(string id)
        {
            var viewmodel = this.userService.GetContactDetailsForUser(id);
            return this.View("Contact", viewmodel);
        }
    }
}


