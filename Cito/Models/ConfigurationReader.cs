using System;
using System.Configuration;

namespace Cito.Models
{
    public class ConfigurationReader : IConfigurationReader
    {
        public static IConfigurationReader Create()
        {
            return new ConfigurationReader();
        }

        public int OffersValidationDaysCount => GetConfigurationInt("OfferValidInDays");

        public string MicrosoftClientId => GetConfigurationString("MicrosoftClientId");
        public string MicrosoftclientSecret => GetConfigurationString("MicrosoftclientSecret");
        public string TwitterConsumerKey => GetConfigurationString("TwitterConsumerKey");
        public string TwitterconsumerSecret => GetConfigurationString("TwitterconsumerSecret");
        public string FacebookAppId => GetConfigurationString("FacebookAppId");
        public string FacebookAppSecret => GetConfigurationString("FacebookAppSecret");
        public string GoogleClientId => GetConfigurationString("GoogleClientId");
        public string GoogleClientSecret => GetConfigurationString("GoogleClientSecret");


        private int GetConfigurationInt(string key) => int.Parse(ConfigurationManager.AppSettings[key]);
        private string GetConfigurationString(string key) => ConfigurationManager.AppSettings[key];
        private string GetEnviromentString(string key) => Environment.GetEnvironmentVariable(key);

    }
}