﻿namespace Cito.Models
{
    using System;

    public class SellOffer
    {
        public long Id { get; set; }
        public DateTime Created { get; set; }
        public string OwnerId { get; set; }
        public long BuyOfferId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public string DeliveryMethod { get; set; }

        public virtual BuyOffer BuyOffer { get; set; }
        public virtual ApplicationUser Owner { get; set; }
    }
}