﻿namespace Cito.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using Controllers;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Repositories;
    using ViewModels;

    public class ApplicationDbContext :
        IdentityDbContext<ApplicationUser>,
        IOfferRepository,
        IUserRepository
    {
        private readonly IConfigurationReader configuration;

        public ApplicationDbContext(IConfigurationReader configuration)
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            this.configuration = configuration;
            this.Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<BuyOffer> BuyOffers { get; set; }
        public DbSet<SellOffer> SellOffers { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext(ConfigurationReader.Create());
        }

        public DataOperationResult<long> SaveNewBuyOffer(BuyOffer model)
        {
            this.BuyOffers.Add(model);
            this.SaveChanges();
            return DataOperationResult<long>.Success(model.Id);
        }

        public BuyOffer GetOfferWithOwnerAndSellOffers(long id)
        {
            var buyOffer = this.BuyOffers
                               .Include(x => x.Owner)
                               .Include(x => x.SellOffers)
                               .Include(x => x.SellOffers.Select(o => o.Owner))
                               .Single(x => x.Id == id);
            return buyOffer;
        }

        public DataOperationResult<long> SaveNewSellOffer(SellOffer model)
        {
            this.SellOffers.Add(model);
            this.SaveChanges();
            return DataOperationResult<long>.Success(model.Id);
        }

        public List<BuyOffer> GetActiveBuyOffersForUser(string currentUserId)
        {
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            DateTime validDate = DateTime.Now.Subtract(TimeSpan.FromDays(offersValidationDaysCount));
            var buyOffers = this.BuyOffers.Where(offer => offer.OwnerId == currentUserId)
                .Where(offer => offer.Created > validDate)
                .ToList();
            return buyOffers;
        }

        public List<BuyOffer> GetActiveBuyOffers(DisplayListQueryParameters query)
        {
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            DateTime validDate = DateTime.Now.Subtract(TimeSpan.FromDays(offersValidationDaysCount));
            var buyOffers = this.BuyOffers
                .Where(o => o.Created > validDate);
            var orderedOffers = OrderOffers(buyOffers, query.Order)
                .Skip((query.Page - 1) * query.Show)
                .Take(query.Show)
                .ToList();
            return orderedOffers;
        }

        private IQueryable<BuyOffer> OrderOffers(IQueryable<BuyOffer> buyOffers, OfferOrder order)
        {
            switch (order)
            {
                case OfferOrder.CreatedAscending:
                    return buyOffers.OrderBy(o => o.Created);
                case OfferOrder.CreatedDescending:
                    return buyOffers.OrderByDescending(o => o.Created);
                case OfferOrder.NameAscending:
                    return buyOffers.OrderBy(o => o.Title);
                case OfferOrder.NameDescending:
                    return buyOffers.OrderByDescending(o => o.Title);
                case OfferOrder.CityAscending:
                    return buyOffers.OrderBy(o => o.DeliverToCity);
                case OfferOrder.CityDescending:
                    return buyOffers.OrderByDescending(o => o.DeliverToCity);
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }
        }

        private Expression<Func<BuyOffer, object>> OrderPredicate(OfferOrder order)
        {
            switch (order)
            {
                case OfferOrder.CreatedAscending:
                    return offer => (DateTime)offer.Created;
                case OfferOrder.CreatedDescending:
                    return offer => (DateTime)offer.Created;
                case OfferOrder.NameAscending:
                    return offer => (string)offer.Title;
                case OfferOrder.NameDescending:
                    return offer => (string)offer.Title;
                case OfferOrder.CityAscending:
                    return offer => (string)offer.DeliverToCity;
                case OfferOrder.CityDescending:
                    return offer => (string)offer.DeliverToCity;
                default:
                    throw new ArgumentOutOfRangeException(nameof(order), order, null);
            }
        }

        public List<SellOffer> GetActiveSellOffersForUser(string currentUserId)
        {
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            DateTime validDate = DateTime.Now.Subtract(TimeSpan.FromDays(offersValidationDaysCount));
            var buyOffers = this.SellOffers.Include(offer => offer.BuyOffer).Where(offer => offer.OwnerId == currentUserId).Where(offer => offer.BuyOffer.Created > validDate).ToList();
            return buyOffers;
        }

        public long GetCountOfActiveBuyOffers()
        {
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            DateTime validDate = DateTime.Now.Subtract(TimeSpan.FromDays(offersValidationDaysCount));
            var buyOffers = this.BuyOffers.Count(o => o.Created > validDate);
            return buyOffers;
        }

        public List<BuyOffer> GetLastesOffersWithBuyerInfo(int count)
        {
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            DateTime validDate = DateTime.Now.Subtract(TimeSpan.FromDays(offersValidationDaysCount));
            return this.BuyOffers
                .Where(offer => offer.Created > validDate)
                .Include(offer => offer.Owner)
                .OrderByDescending(offer => offer.Created)
                .Take(count)
                .ToList();
        }

        public OperationResult ChangeUserName(string userId, string newUserName)
        {
            var applicationUser = this.Users.First(user => user.Id == userId);
            applicationUser.UserName = newUserName;
            this.SaveChanges();
            return OperationResult.Success();
        }

        public OperationResult ChangeUserEmail(string userId, string newEmailAddress)
        {
            var applicationUser = this.Users.First(user => user.Id == userId);
            applicationUser.Email = newEmailAddress;
            this.SaveChanges();
            return OperationResult.Success();
        }
    }
}