namespace Cito.Models
{
    public enum OfferOrder
    {
        CreatedAscending = 0,
        CreatedDescending = 1,
        NameAscending = 2,
        NameDescending = 3,
        CityAscending = 4,
        CityDescending = 5
    }
}