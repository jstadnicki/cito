﻿namespace Cito.Models
{
    using System;
    using System.Collections.Generic;
    using Controllers;

    public class BuyOffer
    {
        public long Id { get; set; }
        public DateTime Created { get; set; }
        public string OwnerId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string DeliverToCity { get; set; }

        public virtual ApplicationUser Owner { get; set; }
        public virtual List<SellOffer> SellOffers { get; set; }
    }
}