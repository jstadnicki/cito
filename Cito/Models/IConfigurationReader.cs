namespace Cito.Models
{
    public interface IConfigurationReader
    {
        int OffersValidationDaysCount { get; }
        string MicrosoftClientId { get;  }
        string MicrosoftclientSecret { get;  }
        string TwitterConsumerKey { get;  }
        string TwitterconsumerSecret { get;  }
        string FacebookAppId { get;  }
        string FacebookAppSecret { get;  }
        string GoogleClientId { get;  }
        string GoogleClientSecret { get;  }
    }
}