﻿using Cito.Controllers;

namespace Cito.Models
{
    public class DisplayListQueryParameters
    {
        public DisplayListQueryParameters()
        {
            this.Show = 15;
            this.Page = 1;
            this.Order = OfferOrder.CreatedAscending;
        }

        public OfferOrder Order { get; set; }
        public int Show { get; set; }
        public int Page { get; set; }
    }
}