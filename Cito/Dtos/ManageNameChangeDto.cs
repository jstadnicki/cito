using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Cito.Dtos
{
    public class ManageNameChangeDto
    {
        [Required]
        [MinLength(3)]
        public string NewUserName { get; set; }
    }
}