﻿namespace Cito.Dtos
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class NewSellOfferDto
    {
        public NewSellOfferDto() : this(0)
        {

        }
        public NewSellOfferDto(long id)
        {
            this.BuyOfferId = id;
        }

        public long BuyOfferId { get; set; }

        [Required]
        [MinLength(10)]
        [Display(Name = "Szczegóły oferty")]
        public string Description { get; set; }

        [Required]
        [Range(typeof(decimal), "0", "100000000000")]
        [Display(Name = "Cena")]
        public decimal Price { get; set; }

        [Required]
        [MinLength(10)]
        [Display(Name = "Sposób dostawy")]
        public string DeliveryMethod { get; set; }
    }
}