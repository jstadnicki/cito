﻿namespace Cito.Dtos
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class NewBuyOfferDto
    {
        [Display(Name = "Nazwa")]
        [Required]
        [MinLength(5)]
        public string Title { get; set; }

        [MinLength(10)]
        [Required]
        [Display(Name = "Opis przedmiotu")]
        public string Description { get; set; }

        [Display(Name = "Miasto dostawy")]
        [Required]
        [MinLength(3)]
        public string DeliverToCity { get; set; }
    }
}