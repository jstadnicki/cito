using System.ComponentModel.DataAnnotations;

namespace Cito.Dtos
{
    public class ManagePhoneChangeDto
    {
        [Required]
        public string NewPhoneNumber { get; set; }
    }
}