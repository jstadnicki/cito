using System.ComponentModel.DataAnnotations;

namespace Cito.Dtos
{
    public class ManageEmailChangeDto
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string NewEmailAddress { get; set; }
    }
}