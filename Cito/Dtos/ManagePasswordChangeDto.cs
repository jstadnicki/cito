using System.ComponentModel.DataAnnotations;

namespace Cito.Dtos
{
    public class ManagePasswordChangeDto
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword")]
        public string NewPasswordConfirm { get; set; }
    }
}