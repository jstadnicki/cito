﻿using System.Collections.Generic;
using Cito.Controllers;
using Cito.Models;
using Cito.ViewModels;

namespace Cito.Repositories
{
    public interface IOfferRepository
    {
        DataOperationResult<long> SaveNewBuyOffer(BuyOffer model);
        DataOperationResult<long> SaveNewSellOffer(SellOffer model);

        BuyOffer GetOfferWithOwnerAndSellOffers(long id);

        List<BuyOffer> GetActiveBuyOffersForUser(string currentUserId);
        List<BuyOffer> GetActiveBuyOffers(DisplayListQueryParameters query);
        List<SellOffer> GetActiveSellOffersForUser(string currentUserId);
        long GetCountOfActiveBuyOffers();
        List<BuyOffer> GetLastesOffersWithBuyerInfo(int count);
    }
}