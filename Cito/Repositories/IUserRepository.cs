using Cito.ViewModels;

namespace Cito.Repositories
{
    public interface IUserRepository
    {
        OperationResult ChangeUserName(string userId, string newUserName);
        OperationResult ChangeUserEmail(string userId, string newEmailAddress);
    }
}