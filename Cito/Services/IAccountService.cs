using Cito.Dtos;
using Cito.ViewModels;

namespace Cito.Services
{
    public interface IAccountService
    {
        ManageIndexViewModel GetCurrentUserDetails();
        ManageNameChangeViewModel GetChangeNameViewModel();
        OperationResult ChangeCurrentUsername(ManageNameChangeDto model);
        ManageEmailChangeViewModel GetChangeEmailViewModel();
        ManagePasswordChangeViewModel GetChangePasswordViewModel();
        ManagePhoneChangeViewModel GetPhoneChangeViewModel();
        OperationResult ChangeCurrentUserEmail(ManageEmailChangeDto model);
        OperationResult ChangeCurrentUserpassword(ManagePasswordChangeDto model);
        OperationResult ChangeCurrentUserPhone(ManagePhoneChangeDto model);
    }
}