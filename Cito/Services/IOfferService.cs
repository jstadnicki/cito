﻿namespace Cito.Services
{
    using Dtos;
    using Models;
    using ViewModels;

    public interface IOfferService
    {
        DataOperationResult<long> AddNewBuy(NewBuyOfferDto model);
        BuyOfferDetailsViewModel GetBuyOfferDetailsViewModel(long id);
        DataOperationResult<long> AddNewSell(NewSellOfferDto model);
        OfferListViewModel GetActiveBuyOffers(DisplayListQueryParameters query);
        HomeIndexViewModel GetLatestOffers(int count);
    }
}