﻿namespace Cito.Services
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Dtos;
    using Models;
    using Repositories;
    using ViewModels;

    public class OfferService : IOfferService
    {
        private readonly IAccount account;
        private readonly IOfferRepository offerRepository;
        private readonly IConfigurationReader configuration;

        public OfferService(IOfferRepository offerRepository, IAccount account, IConfigurationReader configuration)
        {
            this.offerRepository = offerRepository;
            this.account = account;
            this.configuration = configuration;
        }

        public DataOperationResult<long> AddNewBuy(NewBuyOfferDto dto)
        {
            var model = Mapper.Map<NewBuyOfferDto, BuyOffer>(dto);
            model.Created = DateTime.Now;
            model.OwnerId = this.account.GetCurrentUserId();

            var result = this.offerRepository.SaveNewBuyOffer(model);
            return result;
        }

        public BuyOfferDetailsViewModel GetBuyOfferDetailsViewModel(long id)
        {
            var offerModel = this.offerRepository.GetOfferWithOwnerAndSellOffers(id);
            var viewmodel = Mapper.Map<BuyOffer, BuyOfferDetailsViewModel>(offerModel);
            return viewmodel;
        }

        public DataOperationResult<long> AddNewSell(NewSellOfferDto dto)
        {
            var model = Mapper.Map<NewSellOfferDto, SellOffer>(dto);
            model.Created = DateTime.Now;
            model.OwnerId = this.account.GetCurrentUserId();

            var result = this.offerRepository.SaveNewSellOffer(model);
            return result;
        }

        public OfferListViewModel GetActiveBuyOffers(DisplayListQueryParameters query)
        {
            var model = this.offerRepository.GetActiveBuyOffers(query);
            var result = Mapper.Map<List<BuyOffer>, OfferListViewModel>(model);
            var activeOffers = this.offerRepository.GetCountOfActiveBuyOffers();
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            result.BuyOfferList.ForEach(offer => offer.ValidUntil = offer.Created.AddDays(offersValidationDaysCount));
            result.TotalActiveOffers = activeOffers;
            result.ActiveQuery = query;

            return result;
        }

        public HomeIndexViewModel GetLatestOffers(int count)
        {
            var model = this.offerRepository.GetLastesOffersWithBuyerInfo(count);
            var viewmodel = Mapper.Map<List<BuyOffer>, HomeIndexViewModel>(model);
            var offersValidationDaysCount = this.configuration.OffersValidationDaysCount;
            viewmodel.BuyOfferList.ForEach(offer => offer.ValidUntil = offer.Created.AddDays(offersValidationDaysCount));
            return viewmodel;
        }
    }
}