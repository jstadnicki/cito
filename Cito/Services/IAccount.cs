﻿namespace Cito.Services
{
    using Models;

    public interface IAccount
    {
        string GetCurrentUserId();
        ApplicationUser GetUserModel(string userId);
        string GetCurrentUserName();
        string GetCurrentUserEmail();
        string GetCurrentUserPhone();
    }
}