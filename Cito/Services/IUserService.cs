﻿namespace Cito.Services
{
    using ViewModels;

    public interface IUserService
    {
        UserBuyingViewModel GetBuyOffers();
        UserSellingViewModel GetSellOffers();
        UserContactDetailsViewModel GetContactDetailsForUser(string userId);
    }
}