namespace Cito.Services
{
    using System.Collections.Generic;
    using AutoMapper;
    using Models;
    using Repositories;
    using ViewModels;

    public class UserService : IUserService
    {
        private readonly IAccount account;
        private readonly IOfferRepository offerRepository;

        public UserService(IAccount account, IOfferRepository offerRepository)
        {
            this.account = account;
            this.offerRepository = offerRepository;
        }

        public UserBuyingViewModel GetBuyOffers()
        {
            var currentUserId = this.account.GetCurrentUserId();
            var activeBuyOffers = this.offerRepository.GetActiveBuyOffersForUser(currentUserId);
            var userBuyingViewModel = Mapper.Map<List<BuyOffer>, UserBuyingViewModel>(activeBuyOffers);
            return userBuyingViewModel;
        }

        public UserSellingViewModel GetSellOffers()
        {
            var currentUserId = this.account.GetCurrentUserId();
            var activeBuyOffers = this.offerRepository.GetActiveSellOffersForUser(currentUserId);
            var userSellingViewModel = Mapper.Map<List<SellOffer>, UserSellingViewModel>(activeBuyOffers);
            return userSellingViewModel;
        }

        public UserContactDetailsViewModel GetContactDetailsForUser(string userId)
        {
            var model = this.account.GetUserModel(userId);
            var viewmodel = Mapper.Map<ApplicationUser, UserContactDetailsViewModel>(model);
            return viewmodel;
        }
    }
}