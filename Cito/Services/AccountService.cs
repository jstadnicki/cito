using System.Web;
using Cito.Dtos;
using Cito.Models;
using Cito.Repositories;
using Cito.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace Cito.Services
{
    public class AccountService : IAccountService
    {
        private IAccount account;
        private IUserRepository userRepository;

        public AccountService(IAccount account, IUserRepository userRepository)
        {
            this.account = account;
            this.userRepository = userRepository;
        }

        public ManageIndexViewModel GetCurrentUserDetails()
        {
            var user = CurrentApplicationUser();
            var viewmodel = AutoMapper.Mapper.Map<ApplicationUser, ManageIndexViewModel>(user);
            return viewmodel;
        }

        private ApplicationUser CurrentApplicationUser()
        {
            var currentUserId = this.account.GetCurrentUserId();
            var user = this.UserManager.FindById(currentUserId);
            return user;
        }

        public ManageNameChangeViewModel GetChangeNameViewModel()
        {
            var currentUserName = this.account.GetCurrentUserName();
            return new ManageNameChangeViewModel(currentUserName);
        }

        public OperationResult ChangeCurrentUsername(ManageNameChangeDto model)
        {
            var userByName = this.UserManager.FindByName(model.NewUserName);
            var currentUsername = this.AuthenticationManager.User.Identity.Name;

            if (userByName != null && model.NewUserName != currentUsername)
            {
                return OperationResult.Fail("Ta nazwa u�ytkownika jest ju� zaj�ta");
            }

            var result = this.userRepository.ChangeUserName(
                this.AuthenticationManager.User.Identity.GetUserId(), model.NewUserName);
            return result;
        }

        public ManageEmailChangeViewModel GetChangeEmailViewModel()
        {
            var email = this.account.GetCurrentUserEmail();
            return new ManageEmailChangeViewModel(email);
        }

        public ManagePasswordChangeViewModel GetChangePasswordViewModel()
        {
            return new ManagePasswordChangeViewModel();
        }

        public ManagePhoneChangeViewModel GetPhoneChangeViewModel()
        {
            var currentApplicationUser = this.CurrentApplicationUser();
            return new ManagePhoneChangeViewModel(currentApplicationUser.PhoneNumber);
        }

        public OperationResult ChangeCurrentUserEmail(ManageEmailChangeDto model)
        {
            var currentApplicationUser = CurrentApplicationUser();
            var userByEmail = this.UserManager.FindByEmail(model.NewEmailAddress);

            if (userByEmail != null && currentApplicationUser.Email != model.NewEmailAddress)
            {
                return OperationResult.Fail("Ten adres email jest ju� zaj�ty");
            }

            var changeCurrentUserEmail = this.userRepository.ChangeUserEmail(currentApplicationUser.Id, model.NewEmailAddress);
            return changeCurrentUserEmail;
        }

        public OperationResult ChangeCurrentUserpassword(ManagePasswordChangeDto model)
        {
            var userId = this.AuthenticationManager.User.Identity.GetUserId();
            var changePassword = this.UserManager.ChangePassword(userId, model.CurrentPassword, model.NewPassword);
            return changePassword.Succeeded
                ? OperationResult.Success()
                : OperationResult.Fail(string.Join("<br/>", changePassword.Errors));
        }

        public OperationResult ChangeCurrentUserPhone(ManagePhoneChangeDto model)
        {
            var currentApplicationUser = this.CurrentApplicationUser();
            IdentityResult identityResult = null;
            if (string.IsNullOrEmpty(currentApplicationUser.PhoneNumber))
            {
                identityResult = this.UserManager.SetPhoneNumber(currentApplicationUser.Id, model.NewPhoneNumber);
            }
            else
            {
                var token = this.UserManager.GenerateChangePhoneNumberToken(currentApplicationUser.Id,
                    currentApplicationUser.PhoneNumber);
                identityResult = this.UserManager.ChangePhoneNumber(currentApplicationUser.Id, model.NewPhoneNumber, token);
            }

            return identityResult.Succeeded
                ? OperationResult.Success()
                : OperationResult.Fail(string.Join("<br/>", identityResult.Errors));
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
    }
}