namespace Cito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class selloffers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SellOffers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false),
                        OwnerId = c.String(maxLength: 128),
                        BuyOfferId = c.Long(nullable: false),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DeliveryMethod = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BuyOffers", t => t.BuyOfferId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId)
                .Index(t => t.OwnerId)
                .Index(t => t.BuyOfferId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SellOffers", "OwnerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SellOffers", "BuyOfferId", "dbo.BuyOffers");
            DropIndex("dbo.SellOffers", new[] { "BuyOfferId" });
            DropIndex("dbo.SellOffers", new[] { "OwnerId" });
            DropTable("dbo.SellOffers");
        }
    }
}
