namespace Cito.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class start : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BuyOffers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Created = c.DateTime(nullable: false),
                        OwnerId = c.String(maxLength: 128),
                        Title = c.String(),
                        Description = c.String(),
                        DeliverToCity = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.OwnerId)
                .Index(t => t.OwnerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BuyOffers", "OwnerId", "dbo.AspNetUsers");
            DropIndex("dbo.BuyOffers", new[] { "OwnerId" });
            DropTable("dbo.BuyOffers");
        }
    }
}
